<?php

$file = file($argv[1]);
array_shift($file);
$i = 0;
$horizontal = [];
$vertical = [];
array_walk($file, function(string $line) use (&$i, &$horizontal, &$vertical) {
    $parts = explode(' ', $line);
    $align = array_shift($parts);
    $count_tags = array_shift($parts);
    asort($parts);
    $i++;
    if ($align == 'H') {
        $horizontal[] = [
            'index' => $i -1,
            'align' => $align,
            'tags' => $parts,
            'count' => $count_tags
        ];
    } else {
        $vertical[] = [
            'index' => $i -1,
            'align' => $align,
            'tags' => $parts,
            'count' => $count_tags
        ];
    }
    return ;
}, $file);

if (count($vertical) % 2 > 0) {
    array_pop($vertical);
}

usort($vertical, 'sorting');

$vertical_new = [];
for ($x = 0; $x < count($vertical) / 2; $x++) {
    $a = $vertical[$x];
    $b = $vertical[$x + count($vertical) / 2];
    $tags = array_merge($a['tags'], $b['tags']);
    $item = [
        'index' => $a['index'].' '.$b['index'],
        'align' => 'V',
        'tags' => $tags,
        'count' => count($tags)
    ];

    //asort($item['tags']);
    $vertical_new[] = $item;
}

$parsed = array_merge($horizontal, $vertical_new);
usort($parsed, 'sorting');

/* $resultArray = [];
while($item = array_shift($parsed)){
    $resultArray[] = $item;
    $result = findBestMatch($item, $parsed);
    if (!$result) break;
    $resultArray[] = $result;
}

$parsed = $resultArray; */

echo (count($parsed)).PHP_EOL;
array_walk($parsed, function($item) {
    echo $item['index'].PHP_EOL;
});

function sorting($a, $b) {
    //if ($a['count'] == $b['count']) {

        return implode("", array_slice($a["tags"], 0, $a['count']*0.3))
        >= implode("", array_slice($b["tags"], 0, $b['count']*0.3));
    //}
    //return $a['count'] > $b['count'];
}