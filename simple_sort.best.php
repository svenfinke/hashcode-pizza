<?php

$file = file($argv[1]);
array_shift($file);
$i = 0;
$horizontal = [];
$vertical = [];
array_walk($file, function(string $line) use (&$i, &$horizontal, &$vertical) {
    $parts = explode(' ', $line);
    $align = array_shift($parts);
    array_shift($parts);
    asort($parts);
    $i++;
    if ($align == 'H') {
        $horizontal[] = [
            'index' => $i -1,
            'align' => $align,
            'tags' => $parts,
        ];
    } else {
        $vertical[] = [
            'index' => $i -1,
            'align' => $align,
            'tags' => $parts,
        ];
    }
    return ;
}, $file);

if (count($vertical) % 2 > 0) {
    array_pop($vertical);
}

usort($vertical, 'sorting');
usort($horizontal, 'sorting');

echo (count($horizontal) + (count($vertical) / 2)).PHP_EOL;
for ($x = 0; $x < count($vertical); $x = $x + 2) {
    echo $vertical[$x]['index'].' '.$vertical[$x+1]['index'].PHP_EOL;
}
array_walk($horizontal, function($item) {
    echo $item['index'].PHP_EOL;
});

function sorting($a, $b) {
    return implode("", array_slice($a["tags"], 0, 2)).implode("", array_slice($a["tags"], -2, 2))
        >= implode("", array_slice($b["tags"], 0, 2)).implode("", array_slice($b["tags"], -2, 2));
}