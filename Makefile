all: run
run:
	php simple_sort.php a_example.txt > result_a.txt
	php simple_sort.php b_lovely_landscapes.txt > result_b.txt
	php simple_sort.php c_memorable_moments.txt > result_c.txt
	php simple_sort.php d_pet_pictures.txt > result_d.txt
	php simple_sort.php e_shiny_selfies.txt > result_e.txt
	zip src.zip simple_sort.php
sf:
	php simple_sort_sf.php a_example.txt > result_a.txt
	php simple_sort_sf.php b_lovely_landscapes.txt > result_b.txt
	php simple_sort_sf.php c_memorable_moments.txt > result_c.txt
	php simple_sort_sf.php d_pet_pictures.txt > result_d.txt
	php simple_sort_sf.php e_shiny_selfies.txt > result_e.txt
	zip src.zip simple_sort_sf.php